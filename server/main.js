import { Meteor } from 'meteor/meteor'
import { WebApp } from 'meteor/webapp'
import '../imports/api/users'
import '../imports/api/blogs'
import '../imports/startup/simple-schema-config'
import moment from 'moment'

Meteor.startup(() => {

  let momentNow = moment()
  console.log(momentNow.format('h:mm:ss a, MMM Do, YYYY'))

})

