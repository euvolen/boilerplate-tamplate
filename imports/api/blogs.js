import {Mongo} from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'
import {Meteor} from 'meteor/meteor'
import shortid from 'shortid'
import moment from 'moment'
//Should be added to Meteor server!!!

export const Blogs = new Mongo.Collection('blogs')

if (Meteor.isServer) {
  Meteor.publish('blogs.view', function () {
    return Blogs.find({},{ fields: {
      title: 1,
      description: 1,
      body: 1,
      created: 1,
      tags:1,
      lastVisitedAt:1
    }})
  })
  Meteor.publish('blogs.edit', function () {
    return Blogs.find({
     
    })
  })
}

Meteor.methods({
      'blog.insert' (title, description, body, tags) {
        if (!this.userId) {
          throw new Meteor.Error('not-authorized')
        }

        new SimpleSchema({
          title: {
            type: String,
            label: 'Title',
            min:4
          },
          description: {
            type: String,
            label: 'Description',
            min:15
          },
          body: {
            type: String,
            label: 'Description',
            min:100
          },
          tags:{
            type: Array
          },
          "tags.$":{
            type: String
          }
        }).validate({
          title,description,body,tags
        })

        Blogs.insert({
          _id: shortid.generate(),
          title,
          description,
          body,
          tags,
          owner: this.userId,
          visible: true,
          created:moment().format('LLLL'),
          modified: false,
          modifiedAt: null,
          lastVisitedAt: null,
          visitedCount: 0
        })
      },
      'blog.update' (_id, title, description, body, tags, visible) {
        if (!this.userId) {
          throw new Meteor.Error('non-authorized')
        }

        new SimpleSchema({
          title: {
            type: String,
            label: 'Title',
            min:4
          },
          description: {
            type: String,
            label: 'Description',
            min:15
          },
          body: {
            type: String,
            label: 'Description',
            min:100
          },
          tags:{
            type: Array
          },
          'tags.$.tag':{
            type: String
          },
          visible:{
            type: Boolean
          }
        }).validate({
          title,description,body,tags
        })

        Links.update({
          _id,
          userId: this.userId
        }, {
          $set: {
            visible
          }
        })


      },
      'blog.countVisits' (_id) {
       
        new SimpleSchema({
          _id: {
            type: String,
            min: 1,
          }
        }).validate({
          _id
        })

        Blogs.update({
          _id
        }, {
          $set: {
          
            lastVisitedAt: new Date().getTime()
          },
          $inc:{
            visitedCount: 1
          }
        })}



      })