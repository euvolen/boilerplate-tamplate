import {Mongo} from 'meteor/mongo'
import SimpleSchema from 'simpl-schema'
import {Meteor} from 'meteor/meteor'
import shortid from 'shortid'
import moment from 'moment'
//Should be added to Meteor server!!!

export const Pages = new Mongo.Collection('pages')

if (Meteor.isServer) {
  Meteor.publish('pages.view', function () {
    return Pages.find({},{ fields: {
      title: 1,
      description: 1,
      body: 1,
      created: 1,
      tags:1,
      lastVisitedAt:1
    }})
  })
  Meteor.publish('pages.edit', function () {
    return Pages.find({userId:this.userId()},{
     
    })
  })
}