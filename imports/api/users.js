import { Meteor } from 'meteor/meteor'
import SimpleSchema from 'simpl-schema'
import { Accounts } from 'meteor/accounts-base'

  // code to run on server at startup
Accounts.validateNewUser((user)=>{
  const email = user.emails[0].address;


    new SimpleSchema({
      
      email:{
        type: String,
        regEx: SimpleSchema.RegEx.Email
      }
    }).validate({ email}) 
 
  return true
})


Meteor.publish('users.editProfile', function usersProfile() {
  return Meteor.users.find(this.userId, {
    fields: {
      emails: 1,
      profile: 1,
      services: 1,
    },
  });
});
Meteor.publish('users.editProfile.asAdmin', function usersProfile() {
  return Meteor.users.find(this.userId, {
    fields: {
      emails: 1,
      profile: 1,
      services: 1,
      roles:1
    }});
});

Accounts.onCreateUser((options, user) => {
  const userToCreate = user;
  if (options.profile) userToCreate.profile = options.profile;
 // const OAuthProfile = getOAuthProfile(options.profile, userToCreate);
 // if (OAuthProfile) sendWelcomeEmail(userToCreate); // Sent for OAuth accounts only here. Sent for password accounts after email verification (https://cleverbeagle.com/pup/v1/accounts/email-verification).
  userToCreate.roles = ['user']; // Set default roles for new sign ups.
  return userToCreate;
});
