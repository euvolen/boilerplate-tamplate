import React from 'react'
import {Accounts} from 'meteor/accounts-base'
import PropTypes from 'prop-types'
import {Meteor}  from 'meteor/meteor'
import {Tracker} from 'meteor/tracker'

export default class PrivateHeader extends React.Component {

constructor(props){
  super(props)
  this.state={
   
  }
}
  componentDidMount(){
    this.nameTracker = Tracker.autorun(()=>{
    const data = Meteor.user()
    if (data){
      this.setState({email:data.emails[0].address, name:data.profile.name, surname:data.profile.surname,verified:data.emails[0].verified })
    
    }
   
  })
   
    
  }
  componentWillUnmount(){
    this.nameTracker.stop()
  }
  render(){  
    return (
        <div className="header">
        <div className="header__content">
        <h1 className="header__title">{this.props.title}</h1>
        <h1 className="header__title">{this.state.name} {this.state.surname}</h1>
       
        <button className="button button--link-text" onClick={()=>
           Accounts.logout()
        }>Logout</button>
        </div>
        {this.state.verified ? <p className="header__content">  this.state.email </p> : undefined}
        </div>)}
}
 
