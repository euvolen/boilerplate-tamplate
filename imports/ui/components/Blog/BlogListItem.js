import React from 'react'
import PropTypes from 'prop-types'
import {Meteor} from 'meteor/meteor'
import moment from 'moment'

export default class BlogListItem extends React.Component{

    constructor(props){
        super(props)
        this.state={
          
        }
    }

    renderStats(){
        const visitMessage = this.props.visitedCount === 1 ? 'visit' : 'visits'
        let visitedMessage = null

        if (typeof this.props.lastVisitedAt === 'number'){
            visitedMessage =`(visited ${moment(this.props.lastVisitedAt).fromNow()})`
        }
        return (<p className="item__message">{this.props.visitedCount} {visitMessage} {visitedMessage}</p>)
    }
    renderTags(){
       return this.props.tags.map((tag)=><div key={tag}>{tag}</div>)
    }
    render(){
    return <div className="item">
    <h2>{this.props.title}</h2>
    <p className="item__message">{this.props.description}</p>
    <p className="item__message">{this.props.body}</p>
    <div className="item__message">{this.renderTags()} </div>
    {this.renderStats()}    
    </div>}
}

BlogListItem.propTypes = {
    title: PropTypes.string.isRequired,
    description:PropTypes.string.isRequired,
    body:PropTypes.string.isRequired,
    tags:PropTypes.array.isRequired,
};
