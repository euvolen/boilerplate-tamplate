import {Meteor} from 'meteor/meteor'
import React from 'react'

export default class AddBlog extends React.Component{
   constructor(props){
     super(props)
     this.state ={
       err: '',
       description:'',
       body:'',
       title:'',
       tags:'' 
     }
   }
    onSubmit(e){
        const{description}=this.state
        const{title}=this.state
        const{body}=this.state
        const tags = this.state.tags.split(', ')
        e.preventDefault()
        
          Meteor.call('blog.insert', title, description, body, tags,(err,res)=>{
            if(!err){
              this.setState({err: '',
              description:'',
              body:'',
              title:'',
              tags:'' })
            } else {
              this.setState({err: err.reason})
            }

          })
             
       
      }
 
  
    render(){
        return (<div> 
        <h1>Add Post</h1>
        {this.state.err ? <p>{this.state.err}</p>: undefined}
        <form onSubmit={this.onSubmit.bind(this)} className="boxed-view__form">
          <input type="text" 
          placeholder="Title"
          ref='title' 
          value = {this.state.title} 
          onChange={(e)=>{
              this.setState({
                title: e.target.value
              })
          }}/>
           <input type="text" 
          placeholder="Description"
          ref='description' 
          value = {this.state.description} 
          onChange={(e)=>{
              this.setState({
                description: e.target.value
              })
          }}/>
           <input type="text" 
          placeholder="Add text here"
          ref='body' 
          value = {this.state.body} 
          onChange={(e)=>{
              this.setState({
                body: e.target.value
              })
          }}/>
          <input type="text" 
          placeholder="Use comma (', ') to divide tags"
          ref='tags' 
          value = {this.state.tags} 
          onChange={(e)=>{
              this.setState({
                tags: e.target.value
              })
          }}/>
          <button className="button">Add Post</button>
        </form>
        </div>)
    }
}