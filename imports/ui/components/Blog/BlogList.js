import React from 'react'
import {Meteor} from 'meteor/meteor'
import {Tracker} from 'meteor/tracker'
import {Blogs} from '../../../api/blogs'
import BlogListItem from './BlogListItem'


export default class BlogList extends React.Component{

    constructor(props){
        super(props)
        this.state= {
            blogs:[]                    
        }
    }
    componentDidMount(){
        this.linksTracker = Tracker.autorun(()=>{
            Meteor.subscribe('blogs.view')
            const blogs = Blogs.find({
               
            }).fetch()
            this.setState({blogs})
          
          })
          
    }
    componentWillUnmount(){
        this.linksTracker.stop()
    }
    
    
    renderLinksListItems(){
      if (this.state.blogs.length===0){
          return <div className="item">
                    <p className="item_status-message">There are posts yet</p>
                 </div>
      }
        
      return  this.state.blogs.map((blog)=>{
            return <BlogListItem key={blog._id}  {...blog}/>
        })
    }

    render (){
        return(
            <div>
                <div>
                    
                    {this.renderLinksListItems()}
                 
                </div>
            </div>
        )
    }
}