import React from 'react'
import {Link} from 'react-router-dom'

export default class PublicHeader extends React.Component {

constructor(props){
  super(props)
  this.state={
   
  }
}
 
  render(){  
    return (
        <div className="header">
          <div className="header__content">
            <h1>{this.props.title}</h1>
              <div>
               <Link to="/login">Login</Link>
                {" / "}
               <Link to="/signin">Signin</Link>
                
              </div>
          </div>
        </div>)}
}
 
