import React from 'react'
import {Link} from 'react-router-dom'

const NonFound = () =>{
    return <div className="boxed-view">
    <div className="boxed-view__box">  
    <h1>Not Found 404 </h1>
    <p>Hmm...We're unable to find that page</p>
    <Link className="button button--link" to="/">HEAD HOME</Link>
    </div>
    </div>
    }
export default NonFound  