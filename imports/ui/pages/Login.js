import React from 'react'
import  {Meteor} from 'meteor/meteor'
import {Link} from 'react-router-dom'

export default class Login extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      error: ''
    }
  }
  onSubmit(e){
    e.preventDefault()
   let email = this.refs.email.value.trim()    
   let password = this.refs.password.value.trim()    
    
    Meteor.loginWithPassword({email}, password, (err)=>{
      if (err){
        this.setState({error: err.reason})
     }else{
        this.setState({error: ''})
     }
    
    })
   
  }
  render() {
    return(
    <div className='boxed-view'>  
      <div className='boxed-view__box' >
      <h1>Login</h1>

      {this.state.error ? <div>{this.state.error}</div> : undefined}

      <form onSubmit={this.onSubmit.bind(this)} className="boxed-view__form" noValidate >
        <input type='email' ref='email' placeholder='Email' />
        <input type='password' ref='password' placeholder='Password' />
        <button className="button">Login</button>
        
      </form>
      <Link to='/signin'> Join Short Lnk!</Link>
      </div>
    </div>)
  }
}