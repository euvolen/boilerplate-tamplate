import React from 'react'
import {Link} from 'react-router-dom'

import {Accounts }from 'meteor/accounts-base'


export default class Admin extends React.Component{
    constructor(props){
        super(props)
        this.state ={
          error: '',
          users:[]
        }
      }
      componentDidMount(){
        this.linksTracker = Tracker.autorun(()=>{
            Meteor.subscribe('users.editProfile.asAdmin')
            const users = Meteor.users.find({}).fetch()
            this.setState({users})
          
          })
          
    }
    componentWillUnmount(){
        this.linksTracker.stop()
    }


      onSubmit(e){
        e.preventDefault()
       let name = this.refs.name.value.trim()
       let surname = this.refs.surname.value.trim()        
       let email = this.refs.email.value.trim()    
       let password = this.refs.password.value.trim()
       let checkedPassword = this.refs.checkedPassword.value.trim()
       
       if (password.length <9){
         return this.setState({
           error:'Password must be more then 8 characters long'
         })
       }
       if (password == checkedPassword){
        Accounts.createUser({profile:{name, surname}, email:email,password:password},(err)=>{
          if (err){
             this.setState({error: err.reason})
          }else{
             this.setState({error: ''})
          }
      })       
      }
      else{
        return this.setState({
          error:'Passwords unmatched'
        })
      }
      
      }

    renderUsers(){
        return this.state.users.map((user)=>{
            return (<div key={user._id}>
            <p>{user.profile.name}</p>
            <p>{user.profile.surname}</p>
            <p>{user.roles}</p>
            </div>)
        })
    }  

    render(){
      return <div className='boxed-view'> 
       <div className='boxed-view__box' >
        
      <h1>Admin Page</h1>

      {this.state.error ? <p>{this.state.error}</p> : undefined}

      {this.renderUsers()}
     
     <form className="boxed-view__form" onSubmit={this.onSubmit.bind(this)} noValidate>
     <input type='text' ref='name' placeholder='Enter your name'/>
     <input type='text' ref='surname' placeholder='Enter your surname'/>
     <input type='email' ref='email' placeholder='Email'/>
     <input type='password' ref='password' placeholder='Password'/>
     <input type='password' ref='checkedPassword' placeholder='Confirm Password'/>
     <button className="button">Create account</button>
     </form>
    
      </div>
      </div>
    }
  }
  