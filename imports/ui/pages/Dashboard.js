import React from 'react'

import PrivateHeader from '../components/PrivateHeader'
import BlogList from '../components/Blog/BlogList'

import AddBlog from '../components/Blog/AddBlog'

export default () =>{
  
  return (<div> 
    <PrivateHeader title="Dashboard"/>
    <div className="page-content">
      Dashboard page content
    </div>
    <BlogList/>
    <AddBlog/> 
    </div> 
    )
}

