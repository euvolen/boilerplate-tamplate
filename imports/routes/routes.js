import { Meteor } from 'meteor/meteor'
import React from 'react'
import browserHistory from 'history/createBrowserHistory'
import  Signup  from '../ui/pages/Signup'
import  Dashboard  from '../ui/pages/Dashboard'
import { Router, Route, Switch, Redirect } from 'react-router'
import  NotFound  from '../ui/pages/NotFound'
import  Login  from '../ui/pages/Login'
import Index from '../ui/pages/Index'
import Admin from '../ui/pages/Admin'

const unauthenticatedPages = ['/','/signin','/login']
const authenticatedPages = ['/dashboard', '/admin']
const history = browserHistory()

 export const onAuthChange =(isAuthenticated) =>{
    const pathName = history.location.pathname
    const isUnauthenticatedPage = unauthenticatedPages.includes(pathName)
    const isAuthenticatedPage = authenticatedPages.includes(pathName)
    if (pathName==='/admin' && isAuthenticated){
      history.replace('/admin')}
    if (isUnauthenticatedPage&&isAuthenticated){
      history.replace('/dashboard')}
    if (isAuthenticatedPage&&!isAuthenticated){
        history.replace('/')}
 }   
 export const routes =(
  <Router history={history}>
    
    <Switch>
      <Route exact path="/" render={Index}/>
      <Route path='/signin' render={() => 
  (Meteor.userId() ? <Redirect to="/dashboard"/> : <Signup/>)}/>
    <Route path='/login' render={() => 
  (Meteor.userId() ? <Redirect to="/dashboard"/> : <Login/>)}/>
     <Route path='/admin' render={() => 
  (Meteor.userId() ? <Admin/> : <Redirect to="/login"/> )}/>
      <Route path='/dashboard' render={() => 
  (!Meteor.userId() ? <Redirect to="/login"/> : <Dashboard/>)}/>
      <Route path='*' component={NotFound} />
    </Switch>
    
  </Router>)
  
  
